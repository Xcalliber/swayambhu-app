﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    [Header("Screen")]
    public GameObject loginScreen;
    public GameObject menuScreen;

    // Start is called before the first frame update
    void Start()
    {
        ChangeScreen(loginScreen);
    }

    public void ChangeScreen(GameObject currentScreen)
    {
        loginScreen.SetActive(false);
        menuScreen.SetActive(false);

        currentScreen.SetActive(true);
    }
         
}
